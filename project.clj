(defproject testcheck-pl "0.1.0-SNAPSHOT"
            :description "Generative testing of PL Core"

            :dependencies [[org.clojure/clojure "1.6.0"]
                           [org.clojure/test.check "0.5.8"]
                           [http-kit "2.1.18"]
                           [cheshire "5.3.1"]
                           [clj-time "0.8.0"]
                           [digest "1.4.4"]
                           [crypto-random "1.2.0"]]

            :main testcheck-pl.core)


