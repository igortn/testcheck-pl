(ns testcheck-pl.core-test
  (:require [clojure.test :refer :all]
            [clojure.test.check.clojure-test :refer :all]
            [clojure.test.check.properties :as prop]
            [testcheck-pl.core :refer :all]
            [testcheck-pl.gen :as gen]))


;(defspec create-patient-test 5
;         (prop/for-all [params gen/create-patient-form-gen]
;                       (let [result (create-patient params)]
;                         (do
;                           (prn result)
;                           (and (= (:status result) 200) (:success result))))))

;(defspec create-patient-test-1 2
;         (prop/for-all [params (gen/create-patient-form-gen)]
;                       (let [create-result (create-patient params)
;                             patient-id (:patient-id create-result)
;                             find-result (find-patient patient-id)]
;                         ;; assert that the find-result carries the same values that were used to create the patient
;                         (do
;                           (prn find-result)
;                           (and (= (:fName params) (get-in find-result [:name "firstName"]))
;                                (= (:lName params) (get-in find-result [:name "lastName"]))
;                                (= (:mrn params) (:mrn find-result))
;                                (= (:dob params) (:dob find-result)))))))
