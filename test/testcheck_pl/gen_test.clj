(ns testcheck-pl.gen-test
  (:require [clojure.test :refer :all]
            [clojure.test.check.properties :as prop]
            [clojure.test.check.clojure-test :refer :all]
            [testcheck-pl.gen :refer :all]))

;(defspec create-patient-form-gen-test 30
;         (prop/for-all [cpf create-patient-form-gen]
;                       (do (prn cpf) true)))

;(defspec dob-gen-test 20
;         (prop/for-all [dob dob-gen]
;                       (do (prn dob) true)))