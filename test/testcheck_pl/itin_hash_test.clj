(ns testcheck-pl.itin-hash-test
  (:require [clojure.test :refer :all]
            [clojure.test.check.generators :as g]
            [clojure.test.check.clojure-test :refer :all]
            [testcheck-pl.itin-hash :refer :all]))

(def ^:private sample-size 1000)
(def ^:private num-runs 100000)

(deftest hash-unique
  (dotimes [_ num-runs]
    (let [v (g/sample hash-gen sample-size)
          d (distinct v)]
      (is (= (count v) (count d))))))