(ns testcheck-pl.itin-hash
  (:require [clojure.test.check.generators :as g]
            [clj-time.core :as dt]
            [clj-time.coerce :as dt-c]
            [clj-time.format :as dt-f]
            [crypto.random :as crypto]
            [digest]))

(defn- gen-hash
  "Randomly generates pin and calculates hash for a given dob.
  This is not a TestCheck generator, but instead it's a function used in Itinerary."
  [^String dob]
  (let [pin (.toUpperCase (crypto/hex 4))]
    (digest/md5 (str dob pin))))

(def ^:private dob-gen
  (let [start-dt (dt/date-time 1940 1 1)
        end-dt (dt/date-time 2000 12 31)
        start-long (dt-c/to-long start-dt)
        end-long (dt-c/to-long end-dt)
        fmt (dt-f/formatter "yyyy-MM-dd")
        convert (fn [date-long] (dt-f/unparse fmt (dt-c/from-long date-long)))]
    (g/fmap convert (g/choose start-long end-long))))

(def hash-gen
  "TestCheck hash generator."
  (g/fmap gen-hash dob-gen))

