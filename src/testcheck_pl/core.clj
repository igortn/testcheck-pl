(ns testcheck-pl.core
  (:require [testcheck-pl.config :as conf]
            [testcheck-pl.gen :as gen]
            [org.httpkit.client :as http]
            [cheshire.core :as ch]
            [clojure.test.check.properties :as prop]
            [clojure.test.check :as tc]))


(defn- create-pl-entity [rel-path form-params]
  (let [url (str (:url conf/server-info) rel-path)
        opts {:basic-auth  [(:user conf/server-info) (:passwd conf/server-info)]
              :form-params form-params}
        response @(http/post url opts)
        body (ch/parse-string (:body response))
        result {:status  (:status response)
                :success (get body "success")}]
    (if (:success result)
      (assoc result :data (get body "data"))
      (assoc result :errorType (get body "errorType") :errors (get body "errors")))))

(defn create-patient
  "Creates a PL patient. Accepts a 'params' map with optional keys :fName, :lName, :dob, :mrn.
  :dob value must have a format of 'yyyy-MM-ddThh:mm:ss'.
  Returns a result map with status, success and patient id, or errors in the case of failure."
  [params]
  (let [{:keys [fName lName dob mrn]} params
        form-params {:name.firstName      fName
                     :name.lastName       lName
                     :birthDate           dob
                     :medicalRecordNumber mrn}
        result (create-pl-entity "/patient/create" form-params)]
    (if (:success result)
      {:success true, :patient-id (get-in result [:data "patient" "id"])}
      result)))

(defn create-visit
  "Creates a visit for a given patient. Accepts a 'params' map with keys :patient-id, :visit-number,
  :admission-type, :est-admission-time, :arrival-time.
  Returns a result map with status, success and patient id, or errors in the case of failure."
  [params]
  (let [{:keys [patient-id visit-number admission-type est-admission-time arrival-time]} params
        form-params {:patientId patient-id
                     :visitNumber visit-number
                     :admissionType admission-type
                     :estimatedAdmissionTime est-admission-time
                     :arrivalTime arrival-time}
        result (create-pl-entity "/visit/create_unlinked" form-params)]
    (if (:success result)
      {:success true, :visit-id (get-in result [:data "visit" "id"])}
      result)))

(defn create-bed-request
  "Creates a bed request for a given visit. Accepts a 'params' map with keys :visit-id, ...."
  [params]
  )

(defn find-patient
  "Finds a PL patient by a patient id. Returns a result map with keys:
  :status, :success, :id, :name, :mrn, :dob."
  [id]
  (let [url (str (:url conf/server-info) "/patient/findDetails")
        opts {:basic-auth  [(:user conf/server-info) (:passwd conf/server-info)]
              :form-params {:patientId id}}
        response @(http/post url opts)
        body (ch/parse-string (:body response))
        result {:status  (:status response)
                :success (get body "success")}]
    (if (:success result)
      (assoc result :id (get-in body ["data" "patient" "id"])
                    :name (get-in body ["data" "patient" "name"])
                    :mrn (get-in body ["data" "patient" "medicalRecordNumber"])
                    :dob (get-in body ["data" "patient" "birthDate"]))
      (assoc result :errorType (get body "errorType") :errors (get body "errors")))))


