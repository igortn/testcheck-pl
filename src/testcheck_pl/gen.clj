(ns testcheck-pl.gen
  (:require [clojure.test.check.generators :as g]
            [clj-time.core :as dt]
            [clj-time.coerce :as dt-c]
            [clj-time.format :as dt-f]))

(def first-names ["JAMES" "JOHN" "ROBERT" "MICHAEL" "WILLIAM" "DAVID" "RICHARD" "CHARLES" "JOSEPH" "THOMAS" "CHRIS" "DANIEL" "PAUL" "MARK" "GEORGE" "STEVEN" "KEVIN" "FRANK" "ERIC" "ANDREW"
                  "MARY" "LINDA" "JENNIFER" "SUSAN" "LISA" "DOROTHY" "NANCY" "KAREN" "BETTY" "HELEN" "SANDRA" "DONNA" "CAROL" "JESSICA" "SHIRLEY" "AMY" "BRENDA" "ANN" "MARTHA" "JOYCE" "ALICE" "JULIE"])

(def last-names ["SMITH" "JOHNSON" "WILLIAMS" "BROWN" "JONES" "MILLER" "DAVIS" "WILSON" "MOORE" "TAYLOR" "ANDERSON" "THOMAS" "JACKSON" "WHITE" "HARRIS" "MARTIN" "THOMPSON" "ROBINSON" "CLARK" "LEWIS"
                 "LEE" "WALKER" "HALL" "ALLEN" "YOUNG" "KING" "WRIGHT" "HILL" "SCOTT" "GREEN" "ADAMS" "BAKER" "NELSON" "CARTER" "MITCHELL" "ROBERTS" "TURNER" "PHILLIPS" "CAMPBELL" "PARKER" "EVANS"])

(def first-name-gen (g/fmap first-names (g/choose 0 (dec (count first-names)))))

(def last-name-gen (g/fmap last-names (g/choose 0 (dec (count last-names)))))

(def mrn-gen (g/fmap #(str "TEST-" %) (g/choose 10000 99999)))

(def dob-gen
  (let [start-dt (dt/date-time 1920 1 1)
        end-dt (dt/now)
        start-long (dt-c/to-long start-dt)
        end-long (dt-c/to-long end-dt)
        fmt (dt-f/formatter "yyyy-MM-dd")
        convert (fn [date-long] (str (dt-f/unparse fmt (dt-c/from-long date-long)) "T00:00:00"))]
    (g/fmap convert (g/choose start-long end-long))))

(def create-patient-form-gen (g/fmap #(array-map :fName (% 0) :lName (% 1) :mrn (% 2) :dob (% 3)) (g/tuple first-name-gen last-name-gen mrn-gen dob-gen)))